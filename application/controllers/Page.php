<?php
/**
* 
*/
class Page extends CI_Controller
{
	//this is default page
	public function index()
	{ 
		$data['title'] = "新闻"; // Capitalize the first letter
    	$this->load->view('templates/header', $data);
    	$this->load->view('pages/about', $data);
    	$this->load->view('templates/footer', $data);
	}

	// view by route uri
	public function views($page='home')
	{ 
		// 代码设置或者设置php.ini来修改默认时区来避免date()警告
		// date_default_timezone_set('Asia/Shanghai');
		$data['title'] = "新闻"; // Capitalize the first letter
    	$this->load->view('templates/header', $data);
    	$this->load->view('pages/'.$page, $data);
    	$this->load->view('templates/footer', $data);

        // 日历类
  		// $this->load->library('calendar');
		// echo $this->calendar->generate();

		// $this->output->set_content_type('text/plain', 'UTF-8');
		// echo $this->output->get_header('content-type');

		// $this->load->library('encrypt');
		// $msg = 'My secret message';
		// $key = 'super-secret-key'; 
		// $encrypted_string = $this->encrypt->encode($msg, $key);
		// echo $encrypted_string;
		// $decode_string = $this->encrypt->decode($encrypted_string,$key);
		// echo $decode_string;

        
        // 判断版本
		$this->load->library('user_agent'); 
		if ($this->agent->is_browser())
		{
		    $agent = $this->agent->browser().' '.$this->agent->version();
		}
		elseif ($this->agent->is_robot())
		{
		    $agent = $this->agent->robot();
		}
		elseif ($this->agent->is_mobile())
		{
		    $agent = $this->agent->mobile();
		}
		else
		{
		    $agent = 'Unidentified User Agent';
		} 
		echo $agent; 
		echo $this->agent->platform(); // Platform info (Windows, Linux, Mac, etc.)


	}
}